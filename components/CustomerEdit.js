import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { setPropsAsInitial } from './../helpers/setPropsAsInitial';
import CustomerActions from './CustomerActions';

/*const isRequired = value => (
    !value && "Campo requerido"
);*/

const isNumber = value => (
    isNaN(Number(value)) && "El campo debe ser numerico"
);

const validate = values => {
    const error = {};

    if(!values.name) {
        error.name = "El campo nombre es requerido";
    }

    if(!values.dni) {
        error.dni = "El DNI es un campo obligatorio";
    }

    return error;
};

const myField = ({ input, meta, type, label, name }) => (
    <div>
        <label htmlFor={name}>{label}</label><br />
        <input {...input} type={!type ? "text" : type}/>
        {
            meta.touched && meta.error && <span>{meta.error}</span>
        }
    </div>
);

const CustomerEdit = ({ name, dni, age, handleSubmit, submitting, onBack }) => {
    return (
        <div>
            <h2>Edicion del cliente</h2>
            <form onSubmit={handleSubmit}>
                <Field 
                    name="name" 
                    component={myField} 
                    label="Nombre">
                </Field>
                <Field 
                    name="dni" 
                    component={myField} 
                    label="DNI">
                </Field>
                <Field 
                    name="age" 
                    component={myField} 
                    type="number"
                    validate={isNumber}
                    label="Edad">
                </Field>
                <CustomerActions>
                    <button type="submit" disabled={submitting}>Aceptar</button>
                    <button onClick={onBack}>Cancelar</button>
                </CustomerActions>
            </form>
        </div>
    );
};

CustomerEdit.propTypes = {
    name: PropTypes.string,
    dni: PropTypes.string,
    age: PropTypes.number,
    onBack: PropTypes.func.isRequired,
};

const CustomerEditForm = reduxForm(
    { 
        form: 'CustomerEdit',
        validate
    })(CustomerEdit);

export default setPropsAsInitial(CustomerEditForm);